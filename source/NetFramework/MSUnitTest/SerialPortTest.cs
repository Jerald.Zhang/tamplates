﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MSUnitTest
{
    /// <summary>
    /// I use a dataman dm60 to read pn and sn so prepare a hardware before this test
    /// </summary>
    [TestClass]
    public class SerialPortTest
    {
        #region Properties and Contructor

        public SerialPortTest()
        {
            SerialPort = new SerialPort(portName: "COM4", baudRate: 9600, dataBits: 8, parity: Parity.None, stopBits: StopBits.One)
            {
                DtrEnable = true
            };

            SerialPortConnectTimeout = TimeSpan.FromMilliseconds(500);
            SerialPortSendTimeout = TimeSpan.FromMilliseconds(500);
            SerialPortReceiveTimeout = TimeSpan.FromMilliseconds(500);
        }

        public SerialPort SerialPort { get; }

        public TimeSpan SerialPortConnectTimeout { get; set; }
        public TimeSpan SerialPortSendTimeout { get; set; }
        public TimeSpan SerialPortReceiveTimeout { get; set; }

        #endregion
        [TestMethod]
        [DataRow("||>TRIGGER ON\r\n")]
        public void ReadTimeOut_ForAllReadOrOneTimeRead_ForOne(string sendData)
        {
            if (!SerialPort.IsOpen)
                SerialPort.Open();
            var sendBytes = Encoding.UTF8.GetBytes(sendData);

            SerialPort.ReadTimeout = 1000;
            SerialPort.Write(sendBytes, 0, sendBytes.Length);
            Thread.Sleep(100);
            var buffer = new byte[1];
            var watch = new Stopwatch();
            watch.Start();
            while (SerialPort.BytesToRead > 0)
            {
                SerialPort.Read(buffer, 0, buffer.Length);
                Thread.Sleep(600);
                Console.WriteLine($"Each: {watch.ElapsedMilliseconds}");
            }
            Assert.IsTrue(watch.ElapsedMilliseconds > SerialPort.ReadTimeout);
        }

        [TestMethod]
        [DataRow("||>TRIGGER ON\r\n")]
        public async Task ReadTimeOut_ForAllReadOrOneTimeRead_ForAll(string sendData)
        {
            if (!SerialPort.IsOpen)
                SerialPort.Open();

            var sendBytes = Encoding.UTF8.GetBytes(sendData);

            SerialPort.ReadTimeout = 500;
            SerialPort.Write(sendBytes, 0, sendBytes.Length);
            Thread.Sleep(100);

            var buffer = new byte[1];
            var watch = new Stopwatch();
            int time = 0;
            try
            {
                using (var cts = new CancellationTokenSource(1000))
                {
                    while (SerialPort.BytesToRead > 0)
                    {
                        await Task.Delay(501);
                        await SerialPort.BaseStream.ReadAsync(buffer, 0, buffer.Length, cts.Token);
                        time++;
                        Console.WriteLine(time);
                    }
                }
            }
            catch (Exception)
            {
            }
            Assert.IsTrue(time == 1);
        }
    }
}

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NetFrameworkUnitTest.Models.WPF
{
    [TestClass]
    public class DependencyPropertyModel : DependencyObject
    {
        public static readonly DependencyProperty SetTextProperty
            = DependencyProperty.Register("SetText", typeof(string), typeof(DependencyPropertyModel), new PropertyMetadata(null, new PropertyChangedCallback(OnSetTextChanged)));

        public string SetText
        {
            get { return (string)GetValue(SetTextProperty); }
            set { SetValue(SetTextProperty, value); }
        }

        private static void OnSetTextChanged(DependencyObject d,
           DependencyPropertyChangedEventArgs e)
        {
            var dependencyPropertyModel = d as DependencyPropertyModel;
            dependencyPropertyModel.OnSetTextChanged(e);
        }

        private void OnSetTextChanged(DependencyPropertyChangedEventArgs e)
        {
            Console.WriteLine(e.NewValue);
            Console.WriteLine(e.OldValue);
            Console.WriteLine(e.Property.Name);
        }
    }
}

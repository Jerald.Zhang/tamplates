﻿using System;

namespace MSUnitTest.Models.Reflaction
{
    public class Test : ITest
    {
        public Test()
        {
            TestProperty = default(int);
        }
        public Test(int val)
        {
            TestProperty = val;
        }
        public int TestProperty { get; set; }

        public void TestMethod()
        {
            throw new NotImplementedException();
        }

        public int TestMethod(int a)
        {
            return a;
        }

        public object TestMethod(object obj)
        {
            return obj;
        }

        public ITest TestMethod(ITest test)
        {
            return test;
        }
    }
}

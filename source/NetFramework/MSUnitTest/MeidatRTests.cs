﻿using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetFrameworkUnitTest
{
    /// <summary>
    /// MeidatR is a implements of Mediator Pattern in .net.
    /// </summary>
    [TestClass]
    public class MeidatRTests
    {
        public class Ping : IRequest<string> { }
        //构建 消息处理
        public class PingHandler : IRequestHandler<Ping, string>
        {
            public Task<string> Handle(Ping request, CancellationToken cancellationToken)
            {
                return Task.FromResult("Pong");
            }
        }
        //TODO:
        public class Mediator : IMediator
        {
            public Mediator()
            {

            }
            public Mediator(ServiceFactory serviceFactory)
            {
            }

            public Task Publish(object notification, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// TNotification is used for one to many
            /// </summary>
            /// <typeparam name="TNotification"></typeparam>
            /// <param name="notification"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public Task Publish<TNotification>(TNotification notification, CancellationToken cancellationToken = default) where TNotification : INotification
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// IRequest is used for one to one
            /// </summary>
            /// <typeparam name="TResponse"></typeparam>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public Task<TResponse> Send<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }

            public Task<object> Send(object request, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }
        }

        [TestMethod]
        public async Task Demo()
        {
            var mediator = new Mediator();
            var response = await mediator.Send(new Ping());
            Assert.AreEqual(response, "Pong");
        }
    }
}

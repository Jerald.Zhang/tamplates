﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetFrameworkUnitTest
{
    [TestClass]
    public class SocketTests
    {
        //We can thought socket as a object contains two handle, 
        //those two handle reference to two part of steam, one stores remote endpoint data and the other stores local data
        //I don't konw if two data parts all at local computer or not.

        public Socket Server { get; private set; }
        public Socket Client { get; private set; }

        [ClassInitialize]
        public void Initialize()
        {

        }

        [TestMethod]
        public void ServerSocket()
        {
            var server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            server.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8080));
            server.Listen(10);
            Task.Run(() =>
            {
                var clientSocket = server.Accept();
                Read(clientSocket);
            });
        }

        private List<byte> Read(Socket socket)
        {
            var receivedData = new List<byte>();
            var receiveBuffer = new byte[socket.ReceiveBufferSize];

            do
            {
                var length = socket.Receive(receiveBuffer);
                receivedData.AddRange(receiveBuffer.Take(length));
            }
            while (socket.Available >= 1);

            return receivedData;
        }
    }
}

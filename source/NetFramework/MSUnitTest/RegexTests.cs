﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetFrameworkUnitTest
{
    [TestClass]
    public class RegexTests
    {
        [TestMethod]
        public void Regex_MatchedSubexpressions()
        {
            string pattern = @"(\w+)\s(\1)";// \1 means the same value of (\w+), representing a duplicated word
            string input = "He said that that was the the correct answer.";
            var matches = Regex.Matches(input, pattern, RegexOptions.IgnoreCase);
            foreach (Match match in matches)
                Console.WriteLine("Duplicate '{0}' found at positions {1} and {2}.",
                                  //we can see here that only one word matched that's a little strange
                                  //also that index of Groups represents subexpression (\w+), and (\1) not count as a subexpression and can't visit here using a index
                                  match.Groups[1].Value, match.Groups[1].Index, match.Groups[2].Index);
            // The example displays the following output:
            //       Duplicate 'that' found at positions 8 and 13.
            //       Duplicate 'the' found at positions 22 and 26.
        }

        [TestMethod]
        [DataRow("123@abc.com", "abc")]
        [DataRow("123@123.com", "123")]
        public void GetMatchedString(string input, string expected)
        {
            var matched = Regex.Match(input, @"^\w*@(?<matchedName>\w*).com$");

            if (!matched.Success)
                Assert.Fail();

            var actul = matched.Groups["matchedName"].Value;
            Assert.AreEqual(expected, actul);
        }
    }
}

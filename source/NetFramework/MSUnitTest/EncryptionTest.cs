﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace NetFrameworkUnitTest
{
    /// <summary>
    /// 
    /// <see cref=""/>
    /// </summary>
    [TestClass]
    public class EncryptionTest
    {
        #region Document
        /// Encryption class on NetFramework:
        ///     <see cref="Aes"/>, <see cref="DES"/>, <see cref="DSA"/>, <see cref="HMAC"/>, <see cref="MD5"/>, 
        ///     <see cref="Oid"/>, <see cref="RC2"/>, <see cref="Rijndael"/>, <see cref="RSA"/>, 
        ///     <see cref="SHA1"/>, <see cref="SHA256"/>, <see cref="SHA384"/>, <see cref="SHA512"/>, <see cref="TripleDES"/>
        /// Some tool class:
        ///     Random Number Generator (RNG): <see cref="Rfc2898DeriveBytes"/>
        ///
        #endregion

        /// <summary>
        /// Generate a key k1 with password pwd1 and salt salt1.
        /// Generate a key k2 with password pwd1 and salt salt1.
        /// Encrypt data1 with key k1 using symmetric encryption, creating edata1.
        /// Decrypt edata1 with key k2 using symmetric decryption, creating data2.
        /// data2 should equal data1.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="saltLength"></param>
        [TestMethod]
        [DataRow("abc", 8)]
        public void Encryption_TripleDES(string password, int saltLength)
        {
            // Create a byte array to hold the random value. 
            byte[] salt = new byte[saltLength];
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with a random value.
                rngCsp.GetBytes(salt);
            }

            //data can be a string or contents of a file.
            string dataToEnc = "Some test data";
            
            int iterations = 1000;
            //The default iteration count is 1000, so the two methods use the same iteration count.
            var key1 = new Rfc2898DeriveBytes(password, salt, iterations);
            var key2 = new Rfc2898DeriveBytes(password, salt);
            // Encrypt the data.
            TripleDES encAlg = TripleDES.Create();

            encAlg.Key = key1.GetBytes(16);
            var encryptionStream = new MemoryStream();
            CryptoStream encrypt = new CryptoStream(encryptionStream, encAlg.CreateEncryptor(), CryptoStreamMode.Write);
            var utfDataToEnc = new UTF8Encoding(false).GetBytes(dataToEnc);

            encrypt.Write(utfDataToEnc, 0, utfDataToEnc.Length);
            encrypt.FlushFinalBlock();
            encrypt.Close();
            byte[] encedData = encryptionStream.ToArray();
            key1.Reset();

            TripleDES decAlg = TripleDES.Create();

            // Try to decrypt, thus showing it can be round-tripped.
            decAlg.Key = key2.GetBytes(16);
            decAlg.IV = encAlg.IV;
            MemoryStream decryptionStream = new MemoryStream();
            CryptoStream decrypt = new CryptoStream(decryptionStream, decAlg.CreateDecryptor(), CryptoStreamMode.Write);
            decrypt.Write(encedData, 0, encedData.Length);
            decrypt.Flush();
            decrypt.Close();
            key2.Reset();

            string dataDec = new UTF8Encoding(false).GetString(decryptionStream.ToArray());

            Assert.AreEqual(dataToEnc, dataDec);
        }

        [TestMethod]
        public void Encryption_Aes()
        {
            string original = "Here is some data to encrypt!";
            string password = "this is a password";
            //var salt = new byte[] { 192, 168, 127, 1 };

            // Create a new instance of the Aes
            // class.  This generates a new key and initialization 
            // vector (IV).
            using (Aes myAes = Aes.Create())
            {

                // Encrypt the string to an array of bytes.
                byte[] encrypted = EncryptStringToBytes_Aes(original, myAes.Key, myAes.IV);

                // Decrypt the bytes to a string.
                string roundtrip = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);

                Assert.AreEqual(original, roundtrip);
            }

            //利用类 Rfc2898DeriveBytes 生成随机盐和哈希值
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, 16, 1000))
            {
                //可以直接生成随机盐，随机盐可以避免相同密码的hash值相等
                //随机盐会和hash后的密码拼接后存在数据中，解密的时候会拆出随机盐然后根据随机盐核对密码hash后的值是否相同
                var salt = rfc2898DeriveBytes.Salt;
                var hashedPassword = rfc2898DeriveBytes.GetBytes(32);
            }

            //RNGCryptoServiceProvider 用来生成随机数组的工具
            using (var rNGCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var defaultSalt = new byte[16];
                rNGCryptoServiceProvider.GetBytes(defaultSalt);
                using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, defaultSalt))//默认迭代1000
                {
                    var salt = rfc2898DeriveBytes.Salt;
                    var flag = true;
                    for (int i = 0; i < salt.Length; i++)
                    {
                        if (salt[i] != defaultSalt[i])
                        {
                            flag = false;
                            break;
                        }
                    }
                    var hashedPassword = rfc2898DeriveBytes.GetBytes(32);
                }
            }
        }

        #region private method
        private byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        private string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
        #endregion
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetFrameworkUnitTest
{
    [TestClass]
    public class TaskTests
    {
        [TestMethod]
        public void HowToCancelTask()
        {
            var sw = new Stopwatch();
            sw.Start();
            var cts = new CancellationTokenSource(2000);
            Task.Run(() =>
            {
                Console.WriteLine("Start");
                Thread.Sleep(6000);
                Console.WriteLine("End");
            }, cts.Token);
            cts.Dispose();

            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);//9 milion seconds, no block
            Assert.IsFalse(cts.IsCancellationRequested);
        }
    }
}

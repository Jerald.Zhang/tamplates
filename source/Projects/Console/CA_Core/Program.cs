﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace CA_Core
{
    interface ITransient { }
    class Transient : ITransient { }
    interface ISingleton { }
    class Singleton : ISingleton { }
    interface IScoped { }
    class Scoped : IScoped { }
    class Program
    {
        static void Main(string[] args)
        {
            //注册服务->创建容器->创建对象
            {
                //1、Transient：每次从容器 （IServiceProvider）中获取的时候都是一个新的实例
                //2、Singleton：每次从同根容器中（同根 IServiceProvider）获取的时候都是同一个实例
                //3、Scoped：每次从同一个容器中获取的实例是相同的、
                IServiceCollection services = new ServiceCollection();
                services = services.AddTransient<ITransient, Transient>();
                services = services.AddScoped<IScoped, Scoped>();
                services = services.AddSingleton<ISingleton, Singleton>();
                IServiceProvider serviceProvider = services.BuildServiceProvider();

                Console.WriteLine(ReferenceEquals(serviceProvider.GetService<ITransient>(), serviceProvider.GetService<ITransient>()));
                Console.WriteLine(ReferenceEquals(serviceProvider.GetService<IScoped>(), serviceProvider.GetService<IScoped>()));
                Console.WriteLine(ReferenceEquals(serviceProvider.GetService<ISingleton>(), serviceProvider.GetService<ISingleton>()));

                IServiceProvider serviceProvider1 = serviceProvider.CreateScope().ServiceProvider;
                IServiceProvider serviceProvider2 = serviceProvider.CreateScope().ServiceProvider;

                Console.WriteLine(ReferenceEquals(serviceProvider1.GetService<IScoped>(), serviceProvider1.GetService<IScoped>()));
                Console.WriteLine(ReferenceEquals(serviceProvider1.GetService<IScoped>(), serviceProvider2.GetService<IScoped>()));
                Console.WriteLine(ReferenceEquals(serviceProvider1.GetService<ISingleton>(), serviceProvider2.GetService<ISingleton>()));
            }
            //Logging
            {
                ILoggerFactory loggerFactory = new LoggerFactory().AddConsole().AddDebug();
                ILogger logger = loggerFactory.CreateLogger<Program>();
                logger.LogInformation(
                  "This is a test of the emergency broadcast system.");
            }
        }
    }
}

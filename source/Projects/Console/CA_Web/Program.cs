﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace CA_Web
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Please input the ip address.");
            //var ipAddr = Console.ReadLine();
            //Console.WriteLine("Please input the port NO.");
            //int.TryParse(Console.ReadLine(), out var port);
            //TcpLisener(ipAddr, port);


        }

        public static void TcpLisener(string ipv4, int port)
        {
            IPAddress localAddr = IPAddress.Parse(ipv4);
            TcpListener server = new TcpListener(localAddr, port);

            try
            {
                server.Start();


                String data = null;

                Console.Write("Waiting for a connection... ");
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine("Connected!");
                NetworkStream stream = client.GetStream();

                if (stream.CanSeek)
                {
                    var bytes = new byte[stream.Length];
                    var actualLength = stream.Read(bytes, 0, bytes.Length);
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, actualLength);
                }
                else
                {
                    var bytes = new byte[256];
                    int actualLength = 0;
                    var strB = new StringBuilder();
                    while ((actualLength = stream.Read(bytes, 0, bytes.Length)) > 0)
                        strB.Append(Encoding.ASCII.GetString(bytes, 0, actualLength));
                    data = strB.ToString();
                }


                Console.WriteLine("Received:");
                Console.WriteLine(data);

                Console.WriteLine("Please input a command");
                var command = Console.ReadLine();
                var commandBytes = Encoding.Default.GetBytes(command);
                stream.Write(commandBytes, 0, commandBytes.Length);

                client.Close();
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                server.Stop();
            }

            Console.WriteLine("\nHit enter to continue...");
            Console.Read();
        }
    }
}

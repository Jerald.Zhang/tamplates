﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CA_MultiThread.TAP
{
    public class LongTimeTask
    {
        public Task WorkTAP()
        {
            Console.WriteLine($"Main thread {Thread.CurrentThread.ManagedThreadId} come into the WorkTAP method");
            return Task.Run(() =>
             {
                 Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} come into the method");
                 Thread.Sleep(4000);
                 Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} will throw a exception");
                 throw new Exception($"Thread {Thread.CurrentThread.ManagedThreadId}");
             });
        }

        /// <summary>
        /// Missed Exception
        /// </summary>
        /// <returns></returns>
        public async Task WorkAsync()
        {
            Console.WriteLine($"Main thread {Thread.CurrentThread.ManagedThreadId} come into the WorkAsync method");
            await Task.Delay(4000);
            Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} will throw a exception");
            throw new Exception($"Thread {Thread.CurrentThread.ManagedThreadId}");
        }

        public Thread WorkThread()
        {
            Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} come into the method");
            var thread = new Thread(new ThreadStart(() =>
            {
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} come into the thread object method");
                Thread.Sleep(4000);
                throw new Exception($"Thread {Thread.CurrentThread.ManagedThreadId}");
            }));
            thread.Start();
            return thread;
        }

        public async Task WorkCall()
        {
            try
            {
                await WorkAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

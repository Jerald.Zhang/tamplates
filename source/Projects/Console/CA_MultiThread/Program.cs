﻿using CA_MultiThread.TAP;
using CA_MultiThread.Work;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CA_MultiThread
{
    class Program
    {
        static void Main(string[] args)
        {
            //
            {
                //Task t = Task.Run(() =>
                //{
                //    // Just loop.
                //    int ctr = 0;
                //    for (ctr = 0; ctr <= 100; ctr++)
                //    {
                //        Thread.Sleep(1000);
                //        Console.WriteLine("in for");
                //    }
                //    Console.WriteLine("Finished {0} loop iterations", ctr);
                //});
                //Console.WriteLine("done");
            }
            {
                //Action<object> action = (object obj) =>
                //{
                //    Console.WriteLine("Task={0}, obj={1}, Thread={2}",
                //    Task.CurrentId, obj,
                //    Thread.CurrentThread.ManagedThreadId);
                //};

                //// Create a task but do not start it.
                //Task t1 = new Task(action, "alpha");

                //// Construct a started task
                //Task t2 = Task.Factory.StartNew(action, "beta");
                //// Block the main thread to demonstrate that t2 is executing
                //t2.Wait();

                //// Launch t1 
                //t1.Start();
                //Console.WriteLine("t1 has been launched. (Main Thread={0})",
                //                  Thread.CurrentThread.ManagedThreadId);
                //// Wait for the task to finish.
                //t1.Wait();

                //// Construct a started task using Task.Run.
                //String taskData = "delta";
                //Task t3 = Task.Run(() => {
                //    Console.WriteLine("Task={0}, obj={1}, Thread={2}",
                //                      Task.CurrentId, taskData,
                //                       Thread.CurrentThread.ManagedThreadId);
                //});
                //// Wait for the task to finish.
                //t3.Wait();

                //// Construct an unstarted task
                //Task t4 = new Task(action, "gamma");
                //// Run it synchronously
                //t4.RunSynchronously();
                //// Although the task was run synchronously, it is a good practice
                //// to wait for it in the event exceptions were thrown by the task.
                //t4.Wait();
            }
            {
                //string[] argTs = new string[2];
                //argTs[0] = "http://docs.microsoft.com";
                //if (argTs.Length > 1)
                //{
                //    //GetPageSizeAsync(argTs[0]).Wait();
                //    GetPageSizeAsync(argTs[0]);
                //    Console.WriteLine("Main thread don't wait！");

                //    Task.Run(() => 
                //    {
                //        Console.WriteLine($"call thread {Thread.CurrentThread.ManagedThreadId} start to wait！");
                //        GetPageSizeAsync("http://docs.microsoft.com").Wait();
                //        Console.WriteLine($"call thread {Thread.CurrentThread.ManagedThreadId} stop waitting！");
                //    });
                //    Console.WriteLine("Main thread finished！");
                //}

                //else
                //    Console.WriteLine("Enter at least one URL on the command line.");
            }
            {//Is await in loop parallelly run or one by one： one by one
                //Task.Run(() =>
                //{
                //    var taskOne = Task.Run(() =>
                //    {
                //        // await Task.Delay(10000) equals 切换线程 + Thread.Sleep(10000) 
                //        var waitTask = Task.Delay(10000);
                //        Console.WriteLine("Task one finished, Order: 2 or 3");
                //        Console.WriteLine($"Is waitTask finished: {waitTask.IsCompleted}");
                //    });
                //    var taskTwo = Task.Run(() =>
                //    {
                //        Task.Delay(10000);
                //        Console.WriteLine("Task two finished, Order: 2 or 3");
                //    });
                //    Task.WaitAll(taskOne, taskTwo);
                //    Console.WriteLine("Order: 4");
                //});

                //Console.WriteLine("Order: 1");
            }
            {
                //var task = Task.Run(() =>
                //{
                //    Thread.Sleep(4000);
                //    try
                //    {
                //        Console.WriteLine($"Theard:{Thread.CurrentThread.ManagedThreadId} will throw a exception");

                //        throw new Exception();
                //    }
                //    catch (Exception)
                //    {
                //        Console.WriteLine($"Theard:{Thread.CurrentThread.ManagedThreadId} has handle the exception");
                //    }
                //});

                //Console.WriteLine("Main thread reach here without exception");
                //Console.WriteLine("But soon the other thread will throw out the exception, cause a break down");
            }
            {
                //TaskScheduler.UnobservedTaskException +=
                //    (_, ev) => Console.WriteLine(ev.Exception.Message);
                //var work = new LongTimeTask();
                //{
                //    //work.WorkTAP();
                //}
                //{
                //    //Task won't throw an exception, only stored in property Task.Exception
                //    var task = work.WorkCall();
                //}
                //{
                //    // work.WorkThread();
                //}
                //Console.WriteLine($"Main Thread{Thread.CurrentThread.ManagedThreadId} finished");
            }
            //while (true)
            //{
            //    Thread.Sleep(1000);
            //    GC.Collect();
            //}
            {
                var work = new LongTimeWork();
                work.UseAwait();
                work.UseWaitAll();

            }
            Console.ReadKey();
        }
        private static async Task GetPageSizeAsync(string url)
        {
            Console.WriteLine($"Working thread is {Thread.CurrentThread.ManagedThreadId}");
            var client = new HttpClient();
            var uri = new Uri(Uri.EscapeUriString(url));
            byte[] urlContents = await client.GetByteArrayAsync(uri);
            Console.WriteLine($"{url}: {urlContents.Length / 2:N0} characters");
        }



    }
}

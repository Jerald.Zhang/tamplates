﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CA_MultiThread.Work
{
    public class LongTimeWork
    {
        public async Task WorkAsync(string method)
        {
            await Task.Run(() =>
             {
                 var sw = new Stopwatch();
                 sw.Start();
                 Thread.Sleep(300);
                 sw.Stop();
                 Console.WriteLine($"WorkAsync in {method} use Milliseconds: {sw.ElapsedMilliseconds}");
             });
        }

        public async Task UseAwait()
        {
            var sw = new Stopwatch();
            sw.Start();
            await WorkAsync("UseAwait");
            await WorkAsync("UseAwait");
            await WorkAsync("UseAwait");
            await WorkAsync("UseAwait");
            await WorkAsync("UseAwait");
            sw.Stop();
            Console.WriteLine($"UseAwait use Milliseconds: {sw.ElapsedMilliseconds}");
        }
        public void UseWaitAll()
        {
            var sw = new Stopwatch();
            sw.Start();
            var tasks = new List<Task>();
            tasks.Add(WorkAsync("UseWaitAll"));
            tasks.Add(WorkAsync("UseWaitAll"));
            tasks.Add(WorkAsync("UseWaitAll"));
            tasks.Add(WorkAsync("UseWaitAll"));
            tasks.Add(WorkAsync("UseWaitAll"));
            Task.WaitAll(tasks.ToArray());
            sw.Stop();
            Console.WriteLine($"UseWaitAll use Milliseconds: {sw.ElapsedMilliseconds}");
        }
    }
}

﻿
using CA_Learn.Reflaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace CA_Learn
{
    class Program_Reflaction
    {
        static void T_Main()
        {
            Type testType = typeof(Test);
            Assembly testAssembly = testType.Assembly;
            Module testModule = testType.Module;
            PropertyInfo testProperty = testType.GetProperty("TestProperty");
            MethodInfo testMethod = testType.GetMethod("TestMethod");
            ConstructorInfo[] constructorInfos = testType.GetConstructors();
            var defaultConstructor = constructorInfos.OrderBy(ctr => ctr.GetParameters().Count()).First();
            var ctrParameters = defaultConstructor.GetParameters().ToList();
            dynamic container = new object();
            var ctrParameterInstances = new List<object>();
            ctrParameters.ForEach(ctrP => ctrParameterInstances.Add(container.FindInstance(ctrP.ParameterType)));

            //动态构造TestMethod
            //TypeBuilder tb= testType.
            {
                //动态创建程序集
                AssemblyName assemblyName = new AssemblyName("MyDynamicAssembly");
                AssemblyBuilder dynamicAssembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);
               
                //动态创建模块
                ModuleBuilder moduleBuilder = dynamicAssembly.DefineDynamicModule(assemblyName.Name, assemblyName.Name + ".dll");
              
                //动态创建类MyClass
                TypeBuilder tb = moduleBuilder.DefineType("MyClass", TypeAttributes.Public);

                //动态创建字段
                FieldBuilder fb = tb.DefineField("", typeof(System.String), FieldAttributes.Private);

                //动态创建构造函数
                Type[] clorType = new Type[] { typeof(System.String) };
                ConstructorBuilder cb1 = tb.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, clorType);
                //生成指令
                ILGenerator ilg = cb1.GetILGenerator();//生成 Microsoft 中间语言 (MSIL) 指令
                ilg.Emit(OpCodes.Ldarg_0);
                ilg.Emit(OpCodes.Call, typeof(object).GetConstructor(Type.EmptyTypes));
                ilg.Emit(OpCodes.Ldarg_0);
                ilg.Emit(OpCodes.Ldarg_1);
                ilg.Emit(OpCodes.Stfld, fb);
                ilg.Emit(OpCodes.Ret);

                //动态创建属性
                PropertyBuilder pb = tb.DefineProperty("MyProperty", PropertyAttributes.HasDefault, typeof(string), null);

                //动态创建方法
                MethodAttributes getSetAttr = MethodAttributes.Public | MethodAttributes.SpecialName;
                MethodBuilder myMethod = tb.DefineMethod("get_Property", getSetAttr, typeof(string), Type.EmptyTypes);
                //生成指令
                ILGenerator numberGetIL = myMethod.GetILGenerator();
                numberGetIL.Emit(OpCodes.Ldarg_0);
                numberGetIL.Emit(OpCodes.Ldfld, fb);
                numberGetIL.Emit(OpCodes.Ret);

                //保存动态创建的程序集
                dynamicAssembly.Save(assemblyName.Name + ".dll");
            }
            {
                ////动态创建的类类型
                //Type classType = DynamicCreateType();
                ////调用有参数的构造函数
                //Type[] ciParamsTypes = new Type[] { typeof(string) };
                //object[] ciParamsValues = new object[] { "Hello World" };
                //ConstructorInfo ci = classType.GetConstructor(ciParamsTypes);
                //object Vector = ci.Invoke(ciParamsValues);
                ////调用方法
                //object[] methedParams = new object[] { };
                //Console.WriteLine(classType.InvokeMember("get_Property", BindingFlags.InvokeMethod, null, Vector, methedParams));
                //Console.ReadKey();
            }
            {
                var intType = Type.GetType("int");

                object objec = new Object();


                //UInt16


                //var intValue=(int)Activator.CreateInstance(intType);

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Learn.AccessModifiers
{
    public class Point
    {
        public Point()
        {
            PrivateData = "PrivateData";
            ProtectData = "ProtectData";
        }

        private string PrivateData;
        private protected int myValue = 0;
        //in getter setter
        protected string ProtectData
        {
            get
            {
                var point = new Point();

                //private
                this.PrivateData = "";
                point.PrivateData = "";
                //protected
                this.ProtectData = "In Defined Type's Method";
                point.ProtectData = "In Defined Type Method But by its instance";

                return ProtectData;
            }
            set
            {
                var point = new Point();

                //private
                this.PrivateData = "";
                point.PrivateData = "";
                //protected
                this.ProtectData = "In Defined Type's Method";
                point.ProtectData = "In Defined Type Method But by its instance";

                PrivateData = value;
            }
        }

        public void InTypeMethod()
        {
            var point = new Point();

            //private
            this.PrivateData = "";
            point.PrivateData = "";

            //protected
            this.ProtectData = "In Defined Type's Method";
            point.ProtectData = "In Defined Type Method But by its instance";
        }
    }
}

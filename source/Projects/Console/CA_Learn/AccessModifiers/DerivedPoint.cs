﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Learn.AccessModifiers
{
    public class DerivedPoint : Point
    {
        public DerivedPoint()
        {

        }

        public string DerivedPublicData { get; set; }

        public void InDerivedTypeMethod()
        {
            //Case One
            this.ProtectData= "In Derived Type Method";

            //Case Two
            var derivedPoint = new DerivedPoint();
            derivedPoint.ProtectData = "In Derived Type Method But By its instance";

            derivedPoint.myValue = 1;
            //Error:
            //var point = new Point();
            //point.ProtectData = "In Derived Type Method But By its instance";
            //point.myValue
        }
    }
}

﻿using CA_Learn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Learn.AccessModifiers
{
    public class Contain
    {
        public Point BaseProperty { get; set; }
        public DerivedPoint DeriveProperty { get; set; }
        void InContainMethod()
        {
           
        }
    }
}

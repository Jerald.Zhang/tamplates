﻿using System;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.WebPages;

namespace CA_Learn.Models
{
    namespace CA_Learn.Models
    {
        public class ViewRenderController : Controller
        {
            // GET: ViewRender
            public void Index()
            {
                var path = string.Empty;
                var viewEngineResult = this.FindView(out path);//查找View
                Render(viewEngineResult, path);//渲染View
            }

            //View的查找，相当于RazorViewEngine
            private ViewEngineResult FindView(out string path)
            {
                var actionName = "Contact";
                var controllerName = "Home";
                var viewLocationFormat = @"~/Views/{1}/{0}.cshtml";
                //根据Controller和Action名称与地址模板组成View相对路径
                path = string.Format(viewLocationFormat, actionName, controllerName);
                //根据文件路径创建RazorView和ViewEngineResult
                var view = new RazorView(this.ControllerContext, path, "", true, null, null);
                return new ViewEngineResult(view, new RazorViewEngine());
            }

            //View的渲染
            private void Render(ViewEngineResult viewEngineResult, string path)
            {
                Type pageType = BuildManager.GetCompiledType(path);//根据对View文件进行编译
                var pageInstance = Activator.CreateInstance(pageType);//创建View文件编译后类型实例
                var webViewPage = this.InitViewPage(pageInstance, viewEngineResult, path);//对实例中相关属性进行初始化
                webViewPage.ExecutePageHierarchy(//完成View的渲染
                    new WebPageContext(this.HttpContext, null, null),
                    this.HttpContext.Response.Output, null);//startpage设置为null，将不会渲染布局页面
            }

            private WebViewPage InitViewPage(object instance, ViewEngineResult viewEngineResult, string path)
            {
                if (!(instance is WebViewPage webViewPage))
                {
                    throw new InvalidOperationException("无效");
                }
                ViewContext viewContext = new ViewContext(this.ControllerContext,
                    viewEngineResult.View,
                    this.ViewData,
                    this.TempData,
                    this.HttpContext.Response.Output);
                webViewPage.VirtualPath = path;

                webViewPage.ViewContext = viewContext;
                webViewPage.ViewData = viewContext.ViewData;
                webViewPage.InitHelpers();
                return webViewPage;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Learn.Reflaction
{
    public interface ITest
    {
        void TestMethod();
        int TestMethod(int a);
        Object TestMethod(Object obj);
        ITest TestMethod(ITest test);
    }
}

﻿using CA_Learn.AccessModifiers;
using CA_Learn.Models;
using ConsoleAppSocketLibs.Interface;
using ConsoleAppSocketLibs.Processor;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Compilation;

namespace CA_Learn
{
    class Program
    {
        static void Main(string[] args)
        {
            {//类型转换
                byte a = 255;
                sbyte b = -1;
                Console.WriteLine(Convert.ToString(b,2));
                byte c = (byte)b;
                Console.WriteLine(Convert.ToString(c, 2));
            }
            {
                //int[] arry = new int[3];
                //Console.WriteLine(arry.Length);
            }
            {
                //double a = 9.99;
                //double b = 0.1;
                //var c = a * b;
                //Console.WriteLine(c);

                //var list = new List<int>(2) { 0, 0 };
                //list[0] = 1;
                //list[1] = 2;

                //foreach (var item in list)
                //{
                //    Console.WriteLine(item);
                //}
                //string sa = "9.99E-1";
                //double sar = Convert.ToDouble(sa);
                //Console.WriteLine(sar.ToString(2));
            }
            {
                //var watch = new Stopwatch();
                //watch.Start();
                //for (int i = 0; i < 10; i++)
                //{
                //    var task= Work();
                //    task.Wait();
                //    Console.WriteLine($"Time cost:{watch.ElapsedMilliseconds}");
                //}
                //Console.WriteLine($"Finished work, totle cost:{watch.ElapsedMilliseconds}");
            }
            {//访问修饰符 protect
                var derivedPoint = new DerivedPoint();
                //Error:
                //Console.WriteLine(derivedPoint.ProtectData);
            }
            {
                //var actionName = "Contact";
                //var controllerName = "Home";
                //var viewLocationFormat = @"~/Views/{1}/{0}.cshtml";
                //var path = string.Format(viewLocationFormat, actionName, controllerName);
                ////I don't know why this doesn't work?
                //var pageType = BuildManager.GetCompiledType(path);
                //Console.WriteLine(pageType);
            }
            {//unsafe code testing
                //unsafe
                //{

                //}
            }
            {//Linq Aggregate
                ////int[] strArry = { 1, 2, 3, 4 };

                ////var result = strArry.Aggregate((accumulation, next) => accumulation + next);
                ////Console.WriteLine(result);
            }
            {//string formati for console app
             // Creates and initializes the CultureInfo which uses the international sort.
                //CultureInfo myCIintl = new CultureInfo("es-ES", false);

                //// Creates and initializes the CultureInfo which uses the traditional sort.
                //CultureInfo myCItrad = new CultureInfo(0x040A, false);

                //// Displays the properties of each culture.
                ////0,1,2 means the order of parameters,while -x means how long it will takes and right or left
                ////0,1,2 表示变量的顺序，后边的负数表示的左对齐并一共占有多少字符不足的用空格表示，正数表示右对齐
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "PROPERTY", "INTERNATIONAL", "TRADITIONAL");
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "CompareInfo", myCIintl.CompareInfo, myCItrad.CompareInfo);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "DisplayName", myCIintl.DisplayName, myCItrad.DisplayName);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "EnglishName", myCIintl.EnglishName, myCItrad.EnglishName);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "IsNeutralCulture", myCIintl.IsNeutralCulture, myCItrad.IsNeutralCulture);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "IsReadOnly", myCIintl.IsReadOnly, myCItrad.IsReadOnly);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "LCID", myCIintl.LCID, myCItrad.LCID);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "Name", myCIintl.Name, myCItrad.Name);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "NativeName", myCIintl.NativeName, myCItrad.NativeName);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "Parent", myCIintl.Parent, myCItrad.Parent);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "TextInfo", myCIintl.TextInfo, myCItrad.TextInfo);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "ThreeLetterISOLanguageName", myCIintl.ThreeLetterISOLanguageName, myCItrad.ThreeLetterISOLanguageName);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "ThreeLetterWindowsLanguageName", myCIintl.ThreeLetterWindowsLanguageName, myCItrad.ThreeLetterWindowsLanguageName);
                //Console.WriteLine("{0,-31}{1,-47}{2,-25}", "TwoLetterISOLanguageName", myCIintl.TwoLetterISOLanguageName, myCItrad.TwoLetterISOLanguageName);
                //Console.WriteLine();

                //// Compare two strings using myCIintl.
                //Console.WriteLine("Comparing \"llegar\" and \"lugar\"");
                //Console.WriteLine("   With myCIintl.CompareInfo.Compare: {0}", myCIintl.CompareInfo.Compare("llegar", "lugar"));
                //Console.WriteLine("   With myCItrad.CompareInfo.Compare: {0}", myCItrad.CompareInfo.Compare("llegar", "lugar"));
            }
            {
                //var set = new List<string>() { "a", "b", "c", "d" };
                //var queryableSet = set.AsQueryable();
                //var qResult = queryableSet.Where(q => q == "a");

            }
            {//类型比对
                //var obj = new Object();
                //var temp = 0d;
                //switch (obj)
                //{
                //    case Rectangle r:
                //        temp = r.Width * r.Height;
                //        break;
                //    case Circle c:
                //        temp = Math.Pow(c.Radius, 2) * Math.PI;
                //        break;
                //    default:
                //        temp = -1;
                //        break;
                //}
                //Console.WriteLine(temp);
            }
            {//Access Modifiers

            }
            Console.ReadKey();
        }
        public static async Task Work()
        {
            await Task.Delay(2000);
        }
        public static double GetArea(object shape)
        {
            switch (shape)
            {
                case Rectangle r:
                    return r.Width * r.Height;
                case Circle c:
                    return Math.Pow(c.Radius, 2) * Math.PI;
                default:
                    return -1;
            }
        }
    }
    class Rectangle
    {
        public double Width { get; set; }
        public double Height { get; set; }
    }

    class Circle
    {
        public double Radius { get; set; }
    }
}

﻿using CA_Learn.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
namespace CA_Learn
{
    [Test(Id = 1)] //Attribute类的子类可以通过命名参数的构造函数直接给公共属性赋值
    class Program_Crypto
    {
        void T_Main(string[] args)
        {
            //利用Rfc2898DeriveBytes类获取Hash值和密码
            {
                byte[] saltGiven = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
                byte[] bytes = new byte[32];
                byte[] bytes2 = new byte[128];
                using (Rfc2898DeriveBytes rfc2898RandomSalt = new Rfc2898DeriveBytes("this is password", 16, 1000))//构造方法1：提供盐长度和迭代次数，会自动生成该长度的随机盐
                {
                    var salt = rfc2898RandomSalt.Salt;//这里是随机盐
                    bytes = rfc2898RandomSalt.GetBytes(32);
                }
                //Asp.Net 盐是随机盐为了知道这个随机盐 框架是用一个49字节的数组 将16 字节的盐+32字节的hash后的密码进行拼接，已方便获取随机盐然后对比传入的密码

                using (Rfc2898DeriveBytes rfc2898GivenSalt = new Rfc2898DeriveBytes("this is password", saltGiven, 1000))//构造方法1：提供盐长度和迭代次数，会自动生成该长度的随机盐
                {
                    var salt = rfc2898GivenSalt.Salt;//这里是传入的盐
                    bytes2 = rfc2898GivenSalt.GetBytes(128);
                }
            }
            {
                string[] passwordargs = args;
                string usageText = "Usage: RFC2898 <password>\nYou must specify the password for encryption.\n";

                if (passwordargs.Length == 0)
                {
                    Console.WriteLine(usageText);
                }
                else
                {
                    string pwd1 = passwordargs[0];
                    Console.WriteLine($"pwd1:{pwd1}");
                    // Create a byte array to hold the random value. 
                    byte[] salt1 = new byte[8];
                    using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
                    {
                        // Fill the array with a random value.
                        rngCsp.GetBytes(salt1);
                    }

                    //data1 can be a string or contents of a file.
                    string data1 = "Some test data";
                    //The default iteration count is 1000 so the two methods use the same iteration count.
                    int myIterations = 1000;
                    try
                    {
                        Rfc2898DeriveBytes k1 = new Rfc2898DeriveBytes(pwd1, salt1,myIterations);
                        Rfc2898DeriveBytes k2 = new Rfc2898DeriveBytes(pwd1, salt1);
                        // Encrypt the data.
                        TripleDES encAlg = TripleDES.Create();
                        encAlg.Key = k1.GetBytes(16);
                        MemoryStream encryptionStream = new MemoryStream();
                        CryptoStream encrypt = new CryptoStream(encryptionStream,encAlg.CreateEncryptor(), CryptoStreamMode.Write);
                        byte[] utfD1 = new System.Text.UTF8Encoding(false).GetBytes(data1);

                        encrypt.Write(utfD1, 0, utfD1.Length);
                        encrypt.FlushFinalBlock();
                        encrypt.Close();
                        byte[] edata1 = encryptionStream.ToArray();
                        k1.Reset();

                        // Try to decrypt, thus showing it can be round-tripped.
                        TripleDES decAlg = TripleDES.Create();
                        decAlg.Key = k2.GetBytes(16);
                        decAlg.IV = encAlg.IV;
                        MemoryStream decryptionStreamBacking = new MemoryStream();
                        CryptoStream decrypt = new CryptoStream(
        decryptionStreamBacking, decAlg.CreateDecryptor(), CryptoStreamMode.Write);
                        decrypt.Write(edata1, 0, edata1.Length);
                        decrypt.Flush();
                        decrypt.Close();
                        k2.Reset();
                        string data2 = new UTF8Encoding(false).GetString(
        decryptionStreamBacking.ToArray());

                        if (!data1.Equals(data2))
                        {
                            Console.WriteLine("Error: The two values are not equal.");
                        }
                        else
                        {
                            Console.WriteLine("The two values are equal.");
                            Console.WriteLine("k1 iterations: {0}", k1.IterationCount);
                            Console.WriteLine("k2 iterations: {0}", k2.IterationCount);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error: ", e);
                    }

                }
            }
            //AES
            {
                string original = "Here is some data to encrypt!";

                // Create a new instance of the Aes
                // class.  This generates a new key and initialization 
                // vector (IV).
                using (Aes myAes = Aes.Create())
                {

                    // Encrypt the string to an array of bytes.
                    byte[] encrypted = EncryptStringToBytes_Aes(original, myAes.Key, myAes.IV);

                    // Decrypt the bytes to a string.
                    string roundtrip = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);

                    //Display the original data and the decrypted data.
                    Console.WriteLine("Original:   {0}", original);
                    Console.WriteLine("Round Trip: {0}", roundtrip);
                }
            }
            {
                Int32[] set1 = { 1, 2, 3 };
                Int32[] set2 = { 2, 3, 4, 5, 6 };
                //交集
                var setResult = set1.Intersect(set2);
                //并集
                setResult = set1.Concat(set2).Distinct();
            }
            //Equals方法值类型默认比较值,引用类型默认比较引用
            {
                double a = 1.11;
                double b = 1.11;
                object oA = "A";
                object oB = oA;
                var hasha = a.GetHashCode();
                var hashb = b.GetHashCode();
                var ifEqual = Object.ReferenceEquals(a, b);
                ifEqual = a.Equals(b);
                ifEqual = Object.ReferenceEquals(oA, oB);
            }
            //
            {
                object obj = new
                {
                    a = 1,
                    b = 2
                };
            }
            //
            {
                var str = nameof(TestClass) + "_" + nameof(TestTwo);
                var testClass = new TestClass();
                var varName = nameof(testClass.TT);
                Console.WriteLine(str);
                Console.WriteLine(varName);
            }
            //Json object serializer
            {
                Student stu = new Student()
                {
                    ID = 1,
                    Name = "曹操",
                    Sex = "男",
                    Age = 1000
                };
                //序列化
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(Student));
                MemoryStream msObj = new MemoryStream();
                //将序列化之后的Json格式数据写入流中
                js.WriteObject(msObj, stu);
                msObj.Position = 0;
                //从0这个位置开始读取流中的数据
                StreamReader sr = new StreamReader(msObj, Encoding.UTF8);
                string json = sr.ReadToEnd();
                sr.Close();
                msObj.Close();
                Console.WriteLine(json);

                //反序列化
                string toDes = json;
                //string to = "{\"ID\":\"1\",\"Name\":\"曹操\",\"Sex\":\"男\",\"Age\":\"1230\"}";
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(toDes)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(Student));
                    Student model = (Student)deseralizer.ReadObject(ms);// //反序列化ReadObject
                    Console.WriteLine("ID=" + model.ID);
                    Console.WriteLine("Name=" + model.Name);
                    Console.WriteLine("Age=" + model.Age);
                    Console.WriteLine("Sex=" + model.Sex);
                }
            }
            //Jsom List<T> serializer
            {
                Student stu = new Student()
                {
                    ID = 1,
                    Name = "曹操",
                    Sex = "男",
                    Age = 1000
                };
                List<Student> students = new List<Student>
                {
                    stu
                };
                //序列化
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(List<Student>));
                MemoryStream msObj = new MemoryStream();
                //将序列化之后的Json格式数据写入流中
                js.WriteObject(msObj, students);
                msObj.Position = 0;
                //从0这个位置开始读取流中的数据
                StreamReader sr = new StreamReader(msObj, Encoding.UTF8);
                string json = sr.ReadToEnd();
                sr.Close();
                msObj.Close();
                Console.WriteLine(json);

                //反序列化
                string toDes = json;
                //string to = "{\"ID\":\"1\",\"Name\":\"曹操\",\"Sex\":\"男\",\"Age\":\"1230\"}";
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(toDes)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(Student));
                    Student model = (Student)deseralizer.ReadObject(ms);// //反序列化ReadObject
                    Console.WriteLine("ID=" + model.ID);
                    Console.WriteLine("Name=" + model.Name);
                    Console.WriteLine("Age=" + model.Age);
                    Console.WriteLine("Sex=" + model.Sex);
                }
                //Newtonsoft.json
                {
                    #region 3.Json.NET序列化
                    List<Student> lstStuModel = new List<Student>()
                    {

                    new Student(){ID=1,Name="张飞",Age=250,Sex="男"},
                    new Student(){ID=2,Name="潘金莲",Age=300,Sex="女"}
                    };

                    //Json.NET序列化
                    string jsonData = JsonConvert.SerializeObject(lstStuModel);
                    Console.WriteLine(jsonData);
                    //Json.NET反序列化
                    //string json2 = @"{ 'Name':'C#','Age':'3000','ID':'1','Sex':'女'}";
                    //Student descJsonStu = JsonConvert.DeserializeObject<Student>(json);//反序列化
                    //Console.WriteLine(string.Format("反序列化： ID={0},Name={1},Sex={2},Sex={3}", descJsonStu.ID, descJsonStu.Name, descJsonStu.Age, descJsonStu.Sex));

                    #endregion
                }

            }
            {
                double b = 1E-20;
                double a = 0.111111;
                Console.WriteLine(a - b);
            }

            //Enum enum-string-value-null all is these
            {

                Console.WriteLine(TestEnum.A.ToString());
                //string to enum
                var enumA = Enum.Parse(typeof(TestEnum), "A");
                Console.WriteLine(enumA);
                //enum underlying type the same as typeof(byte, int or long etc).
                var enum1 = Enum.GetUnderlyingType(typeof(TestEnum));
                Console.WriteLine(enum1 == typeof(int));
                //UnderlyingType num to enum
                var succReturn = Enum.GetName(typeof(TestEnum), 1);
                Console.WriteLine(succReturn);
                {
                    //if value out of range or parse failed return null
                    string failReturn = Enum.GetName(typeof(TestEnum), 1000);
                }

            }

            //Attribute类的子类可以通过命名参数的构造函数直接给公共属性赋值
            {
                //看line 13
            }
            Console.ReadKey();
        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }
    }
}

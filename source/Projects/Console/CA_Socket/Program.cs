using ConsoleAppSocketLibs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CA_Socket
{
    class Program
    {
        static void Main(string[] args)
        {
            var readBlockLenagth = 256;
            var encoding = Encoding.ASCII;
            var endSymbol = "\\";
            var endSymbolBytes = encoding.GetBytes(endSymbol);
            var quit = new List<string>() { "sdc", "shutdown client" };
            var quitString = new StringBuilder();
            foreach (var item in quit)
            {
                quitString.Append(item).Append("/");
            }
#if DEBUG
            var ipv4 = "172.16.0.101";
#else
            Console.WriteLine("Please into the ipv4 address");
            var ipv4 = Console.ReadLine();
#endif

#if DEBUG
            int port = 23;
#else
            Console.WriteLine("Please into the port NO");
            int.TryParse(Console.ReadLine(), out int port);
#endif
            var listenLength = 5;
            IPAddress.TryParse(ipv4, out IPAddress iPAddress);
            var endPoint = new IPEndPoint(iPAddress, port);
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var server = new Server(socket);
            server.Start(endPoint, listenLength);

            Console.WriteLine("Socket start IP: {0}, Port: {1}", ipv4, port);
            Console.Write("Waiting for client to connect...");
            while (true)
            {
                var clientSocket = server.Accept();
                Console.WriteLine("connect!");
                Task.Run(() =>
                {
                    using (clientSocket)
                    {
                        Console.WriteLine($"Waiting for message form client. Input a message and click Enter to send, or input {quitString.ToString()} to quit conversation");
                        Console.Write(Environment.NewLine);
                        Console.WriteLine("------------------------");
                        Console.Write(Environment.NewLine);
                        while (true)
                        {
                            try
                            {
                                if (!clientSocket.ClientSocket.Connected)
                                {
                                    Console.WriteLine("client has shutdown!");
                                    break;
                                }
                                var dataFromClient = clientSocket.Receive(readBlockLenagth);

                                Console.WriteLine($"{clientSocket.ClientSocket.RemoteEndPoint.ToString()}:");
                                Console.WriteLine($"{Encoding.ASCII.GetString(dataFromClient.ToArray())}");
                                Console.WriteLine(Environment.NewLine);

                                Console.WriteLine("Server: ");
                                var message = string.Empty;

                                message = Console.ReadLine();

                                if (quit.Any(q => string.Equals(message, q, StringComparison.OrdinalIgnoreCase)))
                                {
                                    Console.WriteLine("Bye-Bye");
                                    break;
                                }
                                clientSocket.Send(Encoding.ASCII.GetBytes(message + "\r\n"));
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Client error occured and will it shutdown");
                                break;
                            }

                        }

                        Console.Write(Environment.NewLine);
                    }
                });
            }

        }

    }
}

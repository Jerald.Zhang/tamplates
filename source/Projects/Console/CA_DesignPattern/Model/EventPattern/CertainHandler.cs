﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.EventPattern
{
    public class CertainHandler
    {
        public void HandlerTheEvent(object sender, EventArgs e)
        {
            Console.WriteLine("Has handler the event");
        }
    }
}

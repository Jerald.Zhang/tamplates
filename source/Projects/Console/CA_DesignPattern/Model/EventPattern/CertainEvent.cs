﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.EventPattern
{
    public class CertainEvent
    {
        public event EventHandler CertainEventName;
        public void OnCertainEnvent()
        {
            //这里搜集运行时的参数，创建自己的EventArgs
            CertainEventName.Invoke(this, new EventArgs());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.BuilderPattern
{
    public abstract class Builder
    {
        public abstract void BuildPartA();
        public abstract void BuildPartB();
        public abstract Product GetResult();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.BuilderPattern
{
    public class Director
    {
        /// <summary>
        /// Builder uses a complex series of steps
        /// </summary>
        public void Construct(Builder builder)
        {
            builder.BuildPartA();
            builder.BuildPartB();
        }
    }
}

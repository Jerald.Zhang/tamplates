﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.ObserverPattern
{
    public class BinaryObserver : Observer
    {
        public BinaryObserver(Subject subject)
        {
            this.Subject = subject;
            this.Subject.Attach(this);
        }

        public override void Update()
        {
            Console.WriteLine($"Binary String: {Subject.State}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.ObserverPattern
{
    public class OctalObserver : Observer
    {
        public OctalObserver(Subject subject)
        {
            this.Subject = subject;
            this.Subject.Attach(this);
        }
        public override void Update()
        {
            Console.WriteLine($"Octal String: {Subject.State}");
        }
    }
}

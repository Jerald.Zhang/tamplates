﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.ObserverPattern
{
    public class Subject
    {
        public Subject()
        {
            Observers = new List<Observer>();
        }
        private List<Observer> Observers { get; }
        private int _state;
        public int State
        {
            get => _state;
            set
            {
                _state = value;
                NotifyAllObservers();
            }
        }
        public void Attach(Observer observer)
        {
            Observers.Add(observer);
        }

        public void NotifyAllObservers()
        {
            foreach (var observer in Observers)
            {
                observer.Update();
            }
        }
    }
}

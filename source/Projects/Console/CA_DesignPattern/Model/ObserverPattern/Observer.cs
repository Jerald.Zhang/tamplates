﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_DesignPattern.Model.ObserverPattern
{
    public abstract class Observer
    {
        protected Subject Subject { get; set; }

        public abstract void Update();
    }
}

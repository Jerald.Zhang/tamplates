﻿using CA_DesignPattern.Model.BuilderPattern;
using CA_DesignPattern.Model.EventPattern;
using CA_DesignPattern.Model.ObserverPattern;
using System;

namespace CA_DesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //Builder Pattern
            {
                // Builder
                //      为创建一个Product对象的各个部件指定抽象接口；

                // ConcreteBuilder

                //      实现Builder的接口以构造和装配该产品的各个部件
                //      定义并明确它所创建的表示
                //      提供一个检索Product的接口

                //Director

                //      构造一个使用Builder接口的对象；
                //      Hz: 调用Builder的各个接口开始建造对象，执行完成后的结果存在Builder里面

                //Product

                //      表示被构造的复杂对象。ConcreteBuilder创建该产品的内部表示并定义它的装配过程
                //      包含定义组成部件的类，包括将这些部件装配成最终产品的接口

                //在建造者模式中，Director规定了创建一个对象所需要的步骤和次序，Builder则提供了一些列完成这些步骤的方法，ConcreteBuilder给出了这些方法的具体实现，是对象的直接创建者。

                // Create director and builders
                //Director director = new Director();

                //Builder b1 = new ConcreteBuilder1();
                //Builder b2 = new ConcreteBuilder2();

                //// Construct two products
                //director.Construct(b1);
                //Product p1 = b1.GetResult();
                //p1.Show();

                //director.Construct(b2);
                //Product p2 = b2.GetResult();
                //p2.Show();
            }
            //Observer Pattern
            {
                var subject = new Subject();

                new OctalObserver(subject);
                new BinaryObserver(subject);

                Console.WriteLine("First state change: 15");
                subject.State = 15;
                Console.WriteLine("Second state change: 10");
                subject.State = 10;
            }
            //Event Pattern(.Net special)
            {
                //var handler = new CertainHandler();
                //var eventDefineder = new CertainEvent();
                //eventDefineder.CertainEventName +=new EventHandler(handler.HandlerTheEvent);
                //eventDefineder.OnCertainEnvent();
            }
            {

                //switch (Test)
                //{
                //    case Test.A:
                //        break;
                //    case Test.B:
                //        break;
                //    case Test.C:
                //        break;
                //    default:
                //        break;
                //}
            }
        }
    }
    public enum Test
    {
        A,
        B,
        C
    }
}

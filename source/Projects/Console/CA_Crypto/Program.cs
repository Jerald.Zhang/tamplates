﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CA_Crypto
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                //// Generate a key k1 with password pwd1 and salt salt1.
                //// Generate a key k2 with password pwd1 and salt salt1.
                //// Encrypt data1 with key k1 using symmetric encryption, creating edata1.
                //// Decrypt edata1 with key k2 using symmetric decryption, creating data2.
                //// data2 should equal data1.



                ////If no file name is specified, write usage text.
                //if (args.Length == 0)
                //{
                //    Console.WriteLine(usageText);
                //}
                //else
                //{
                //    string pwd1 = args[0];
                //    // Create a byte array to hold the random value. 
                //    byte[] salt1 = new byte[8];
                //    using (var rngCsp = new RNGCryptoServiceProvider())
                //    {
                //        // Fill the array with a random value.
                //        rngCsp.GetBytes(salt1);
                //    }

                //    //data1 can be a string or contents of a file.
                //    string data1 = "Some test data";
                //    //The default iteration count is 1000 so the two methods use the same iteration count.
                //    int myIterations = 1000;
                //    try
                //    {
                //        Rfc2898DeriveBytes k1 = new Rfc2898DeriveBytes(pwd1, salt1, myIterations);
                //        Rfc2898DeriveBytes k2 = new Rfc2898DeriveBytes(pwd1, salt1);
                //        // Encrypt the data.
                //        TripleDES encAlg = TripleDES.Create();
                //        encAlg.Key = k1.GetBytes(16);
                //        var encryptionStream = new MemoryStream();
                //        CryptoStream encrypt = new CryptoStream(encryptionStream, encAlg.CreateEncryptor(), CryptoStreamMode.Write);
                //        byte[] utfD1 = new System.Text.UTF8Encoding(false).GetBytes(data1);

                //        encrypt.Write(utfD1, 0, utfD1.Length);
                //        encrypt.FlushFinalBlock();
                //        encrypt.Close();
                //        byte[] edata1 = encryptionStream.ToArray();
                //        k1.Reset();

                //        // Try to decrypt, thus showing it can be round-tripped.
                //        TripleDES decAlg = TripleDES.Create();
                //        decAlg.Key = k2.GetBytes(16);
                //        decAlg.IV = encAlg.IV;
                //        MemoryStream decryptionStreamBacking = new MemoryStream();
                //        CryptoStream decrypt = new CryptoStream(decryptionStreamBacking, decAlg.CreateDecryptor(), CryptoStreamMode.Write);
                //        decrypt.Write(edata1, 0, edata1.Length);
                //        decrypt.Flush();
                //        decrypt.Close();
                //        k2.Reset();
                //        string data2 = new UTF8Encoding(false).GetString(decryptionStreamBacking.ToArray());

                //        if (!data1.Equals(data2))
                //        {
                //            Console.WriteLine("Error: The two values are not equal.");
                //        }
                //        else
                //        {
                //            Console.WriteLine("The two values are equal.");
                //            Console.WriteLine("k1 iterations: {0}", k1.IterationCount);
                //            Console.WriteLine("k2 iterations: {0}", k2.IterationCount);
                //        }
                //    }
                //    catch (Exception e)
                //    {
                //        Console.WriteLine("Error: ", e);
                //    }

                //}
            }
            string original = "Here is some data to encrypt!";
            string password = "this is a password";
            // var salt = new byte[] { 192, 168, 127, 1 };

            // Create a new instance of the Aes
            // class.  This generates a new key and initialization 
            // vector (IV).
            //using (Aes myAes = Aes.Create())
            //{

            //    // Encrypt the string to an array of bytes.
            //    byte[] encrypted = EncryptStringToBytes_Aes(original, myAes.Key, myAes.IV);

            //    // Decrypt the bytes to a string.
            //    string roundtrip = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);

            //    //Display the original data and the decrypted data.
            //    Console.WriteLine("Original:   {0}", original);
            //    Console.WriteLine("Round Trip: {0}", roundtrip);
            //}

            //利用类 Rfc2898DeriveBytes 生成随机盐和哈希值
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, 16, 1000))
            {
                //可以直接生成随机盐，随机盐可以避免相同密码的hash值相等
                var salt = rfc2898DeriveBytes.Salt;
                var hashedPassword = rfc2898DeriveBytes.GetBytes(32);
            }

            //rNGCryptoServiceProvider 用来生成随机数组的工具
            using (var rNGCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var defaultSalt = new byte[16];
                rNGCryptoServiceProvider.GetBytes(defaultSalt);
                using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, defaultSalt))//默认迭代1000
                {
                    var salt = rfc2898DeriveBytes.Salt;
                    var flag = true;
                    for (int i = 0; i < salt.Length; i++)
                    {
                        if (salt[i] != defaultSalt[i])
                        {
                            flag = false;
                            break;
                        }
                    }
                    var hashedPassword = rfc2898DeriveBytes.GetBytes(32);
                }
            }
        }
        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;

        }
    }
}

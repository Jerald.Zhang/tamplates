﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CA_Ioc.AutofacModels.Interfaces;

namespace CA_Ioc.AutofacModels.Implements
{
    public class ObjWithParameters:IObjWithParameter
    {
        //public ObjWithParameters()
        //{
        //    Time = DateTime.Now;
        //}
        private IOutput _output { get; }
        public ObjWithParameters(string name,DateTime time, IOutput outPut)
        {
            Name = name;
            Time = time;
            _output = outPut;
        }
        public string Name { get; set; }
        public DateTime Time { get; set; }

        public void SayName()
        {
            _output.Write($"Name: {Name}, date time: {Time.ToString()}");
        }
    }
}

﻿using CA_Ioc.AutofacModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Ioc.AutofacModels.Implements
{
    public class ConsoleOutput : IOutput
    {
        public void Write(string content)
        {
            Console.Write(content);
        }
    }
}

﻿using Autofac;
using CA_Autofac.HowAndWhy;
using CA_Ioc.AutofacModels.Implements;
using CA_Ioc.AutofacModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Ioc
{
    class Program
    {
        private static IContainer Container { get; set; }
        static void Main(string[] args)
        {
            //Autofac
            {
                //var builder = new ContainerBuilder();
                //builder.RegisterType<ConsoleOutput>().As<IOutput>();
                //builder.RegisterType<TodayWriter>().As<IDateWriter>();
                //builder.RegisterType<ObjWithParameters>().As<IObjWithParameter>();
                //Container = builder.Build();

                //WriteDate();
            }
            //反射尝试
            {
                //var ct = typeof(ObjWithParameters);
                //var output = Container.Resolve<IOutput>();
                //var obj = Container.Resolve<IObjWithParameter>(new TypedParameter(typeof(string), "Tom"), new NamedParameter("time", DateTime.Now), new NamedParameter("output", output));
                //var method = ct.GetMethod("SayName");
                //method.Invoke(obj, null);
            }
            {//How and Why?
                var container = new Container();
                container.Register<IHow, HowImp>();
                var how = container.GetInstance(typeof(IHow));
                ((HowImp)how).ShowHow();
            }
        }
        public static void WriteDate()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var writer = scope.Resolve<IDateWriter>();
                var output = scope.Resolve<IOutput>();
                var obj = scope.Resolve<IObjWithParameter>(new NamedParameter("name", "Tom"), new NamedParameter("time", DateTime.Now), new NamedParameter("output", output));
                writer.WriteDate();
                obj.SayName();
            }
            var list = new List<Object>();

            var prodution = list.Where(r => true).FirstOrDefault();
            if (prodution != null)
            {

            }
            else
            {

            }
        }
    }
}

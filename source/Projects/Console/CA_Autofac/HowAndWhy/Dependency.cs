﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Autofac.HowAndWhy
{
    public class Dependency
    {
        public Dependency(string property)
        {
            _property = property;
        }

        private readonly string _property;

        public string ShowProperty()
        {
            return _property;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Autofac.HowAndWhy
{
    public class HowImp : IHow
    {
        public HowImp(Dependency dependency)
        {
            _dependency = dependency;
        }

        private readonly Dependency _dependency;


        public void ShowHow()
        {
            Console.WriteLine($"How {_dependency.ShowProperty()}");
        }

        public string Print(string toPrint)
        {
            throw new NotImplementedException();
        }
    }
}

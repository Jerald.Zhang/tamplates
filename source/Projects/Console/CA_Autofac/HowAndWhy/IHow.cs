﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Autofac.HowAndWhy
{
    public interface IHow
    {
        void ShowHow();
        string Print(string toPrint);
    }
}

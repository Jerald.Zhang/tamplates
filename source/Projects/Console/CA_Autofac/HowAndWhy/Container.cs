﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Autofac.HowAndWhy
{
    public class Container
    {
        public Container()
        {
            _registrations = new Dictionary<Type, Func<object>>();
        }

        private readonly Dictionary<Type, Func<object>> _registrations ;

        public void Register<TService, TImpl>() where TImpl : TService
        {
            _registrations.Add(typeof(TService), () => GetInstance(typeof(TImpl)));
        }

        public void Register<TService>(Func<TService> instanceCreator)
        {
            _registrations.Add(typeof(TService), () => instanceCreator());
        }

        public void RegisterSingleton<TService>(TService instance)
        {
            _registrations.Add(typeof(TService), () => instance);
        }

        public void RegisterSingleton<TService>(Func<TService> instanceCreator)
        {
            var lazy = new Lazy<TService>(instanceCreator);//Lazy大概就是这样实现的
            Register(() => lazy.Value);
        }

        public object GetInstance(Type serviceType)
        {
            if (this._registrations.TryGetValue(serviceType, out Func<object> creator))
                return creator();
            else if (!serviceType.IsAbstract)
                return this.CreateInstance(serviceType);
            else
                throw new InvalidOperationException("No registration for " + serviceType);
        }
        //这里用到了迭代，但是没有处理迭代最底层的情况
        //如果依赖是枚举之类的如何处理？
        private object CreateInstance(Type implementationType)
        {
            if (implementationType.IsValueType)
                return 0;
            if (string.Equals(implementationType.Name, typeof(string).Name))
                return string.Empty;
            

            var ctor = implementationType.GetConstructors().Single();
            var parameterTypes = ctor.GetParameters().Select(p => p.ParameterType);
            var dependencies = parameterTypes.Select(t => GetInstance(t)).ToArray();
            return Activator.CreateInstance(implementationType, dependencies);
        }

    }
}

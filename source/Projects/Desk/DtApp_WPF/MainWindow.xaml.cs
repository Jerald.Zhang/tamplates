﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DtApp_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var buttonObj = (Button)sender;
            buttonObj.Background = Brushes.Red;
            IndenpendentWork(sender);
            Thread.Sleep(1000);
            Console.WriteLine("Finished");
        }
        public async Task IndenpendentWork(object button)
        {
            Console.WriteLine("In ShowAsync");
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            await Task.Delay(500);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            var buttonObj = (Button)button;
            buttonObj.Background = Brushes.LightGray;
            await Task.Delay(500);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            buttonObj.Background = Brushes.Yellow;
            await Task.Delay(500);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            buttonObj.Background = Brushes.Green;
        }
        public Task WorkAsync2(object button)
        {
            Console.WriteLine("In ShowAsync2");

            return Task.Run(() =>
            {
                Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(500);
                Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                var buttonObj = (Button)button;
                buttonObj.Background = Brushes.LightGray;
                Thread.Sleep(1000);
                Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                buttonObj.Background = Brushes.Yellow;
                Thread.Sleep(1000);
                Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                buttonObj.Background = Brushes.Green;
            });
        }

        private void MyCustomControl_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("It is the custom routed event of your custom control");
        }
    }
}

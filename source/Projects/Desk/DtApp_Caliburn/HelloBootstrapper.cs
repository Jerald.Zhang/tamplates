﻿using Caliburn.Micro;
using DtApp_Caliburn.ViewModels;
using System.Windows;

namespace DtApp_Caliburn
{
    public class HelloBootstrapper : BootstrapperBase
    {
        public HelloBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();
        }
    }
}

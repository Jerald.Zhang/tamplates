﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DtApp_Caliburn.ViewModels
{
    public class MainWindowViewModel : Screen
    {
        private string _text;
        public string Text
        {
            get => _text;
            set
            {
                Set(ref _text, value);
                NotifyOfPropertyChange(() => CanClickMe);
            }
        }

        public bool CanClickMe
        {
            get => !string.IsNullOrEmpty(_text);
        }

        public void ClickMe()
        {
            MessageBox.Show("CLick");
        }
    }
}

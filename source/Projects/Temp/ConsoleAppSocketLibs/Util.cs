﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSocketLibs
{
    public class Util
    {
        public static bool EndWith(byte[] target, byte[] endSymbol)
        {
            if (target.Length < endSymbol.Length)
                return false;

            var temp = target.Skip(target.Length - endSymbol.Length).Take(endSymbol.Length);
            return temp.SequenceEqual(endSymbol);
        }

        public static bool EndWith(List<byte> target, byte[] endSymbol)
        {
            if (target.Count < endSymbol.Length)
                return false;

            var temp = target.Skip(target.Count - endSymbol.Length).Take(endSymbol.Length);
            return temp.SequenceEqual(endSymbol);
        }

        public static byte[] CutEnd(byte[] target, int length)
        {
            if (target.Length < length)
                throw new InvalidOperationException();
            return target.Take(target.Length - length).ToArray();
        }

        public static List<byte> CutEnd(List<byte> target, int length)
        {
            if (target.Count < length)
                throw new InvalidOperationException();
            return target.Take(target.Count - length).ToList();
        }

        public static byte[] AddEnd(byte[] target, byte[] endSymbol)
        {
            var result = new List<byte>();
            result.AddRange(target);
            result.AddRange(endSymbol);
            return result.ToArray();
        }

        public static byte[] AddEnd(List<byte> target, byte[] endSymbol)
        {

            target.AddRange(endSymbol);
            return target.ToArray();
        }
    }
}

﻿using ConsoleAppSocketLibs.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSocketLibs
{
    public class Client : IDisposable
    {
        public Client(Socket remoteSocket)
        {
            ClientSocket = remoteSocket;
        }

        public Socket ClientSocket { get; }

        public void Connect(IPEndPoint iPEndPoint)
        {
            ClientSocket.Connect(iPEndPoint);
        }

        public List<byte> Receive(int readBlockLength)
        {
            if (readBlockLength < 1)
                readBlockLength = 512;

            var receivedData = new List<byte>();
            var readBlock = new byte[readBlockLength];

            int actualReceivedNumber = 0;
            if (!ClientSocket.Connected)
                return receivedData;
            do
            {
                actualReceivedNumber = ClientSocket.Receive(readBlock);

                //Client sockt has been shutdown
                if (actualReceivedNumber == 0)
                    break;

                if (readBlock.Length > actualReceivedNumber)
                {
                    receivedData.AddRange(readBlock.Take(actualReceivedNumber));
                    break;
                }
                receivedData.AddRange(readBlock);
            } while (ClientSocket.Available > 0);
            return receivedData;
        }

        public byte[] Receive(int readBlockLength, byte[] endSymbolBytes, IMessageProcesser receiveProcesser)
        {
            if (readBlockLength < 1)
                readBlockLength = 512;

            var receivedData = new List<byte>();
            var readBlock = new byte[readBlockLength];

            int actualReceivedNumber = 0;

            do
            {
                if (!ClientSocket.Connected)
                    return receivedData.ToArray();

                actualReceivedNumber = ClientSocket.Receive(readBlock);

                //Client sockt has been shutdown
                if (actualReceivedNumber == 0)
                    break;

                if (readBlock.Length > actualReceivedNumber)
                    readBlock = readBlock.Take(actualReceivedNumber).ToArray();

                receivedData.AddRange(readBlock);

                if (Util.EndWith(receivedData, endSymbolBytes))
                {
                    receivedData = Util.CutEnd(receivedData, endSymbolBytes.Length);
                    break;
                }

            } while (actualReceivedNumber > 0);

            if (receiveProcesser != null)
                return receiveProcesser.Process(receivedData.ToArray());
            else
                return receivedData.ToArray();
        }

        public void Send(byte[] respond)
        {
            ClientSocket.Send(respond);
        }

        public void Send(byte[] respond, IMessageProcesser sendProcesser)
        {
            if (!ClientSocket.Connected)
                throw new InvalidOperationException();

            if (sendProcesser == null)
                ClientSocket.Send(respond);
            else
                ClientSocket.Send(sendProcesser.Process(respond));
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    ClientSocket.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ClientTemp() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}

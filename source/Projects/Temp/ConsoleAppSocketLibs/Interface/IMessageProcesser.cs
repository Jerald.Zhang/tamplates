﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSocketLibs.Interface
{
    public interface IMessageProcesser
    {
        byte[] Process(byte[] sendData);
    }
}

﻿using ConsoleAppSocketLibs.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSocketLibs.Processor
{
    public class SendProcessor : IMessageProcesser
    {
        public SendProcessor(byte[] endSymbol)
        {
            EndSymbol = endSymbol;
        }

        public byte[] EndSymbol { get; set; }

        public byte[] Process(byte[] sendData)
        {
            var processedSendData = new List<byte>();
            processedSendData.AddRange(sendData);
            processedSendData.AddRange(EndSymbol);
            return processedSendData.ToArray();
        }
    }
}

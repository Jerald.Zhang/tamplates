using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;

namespace SerialPortTools.SendCommandToSerialPort
{
    class Program
    {
        static void Main(string[] args)
        {
        Restart:
            var comNames = SerialPort.GetPortNames();
            foreach (var comName in comNames)
            {
                Console.WriteLine(comName);
            }
            Console.WriteLine("input a COM No");
            var comNo = int.Parse(Console.ReadLine());
            var serialPort = new SerialPort($"COM{comNo}", 115200, Parity.None, 8, StopBits.One)
            {
                Handshake = Handshake.XOnXOff,
                RtsEnable = false
            };

            if (!serialPort.IsOpen)
                serialPort.Open();

            SameComNO:

            Console.WriteLine("Input command:");
            var dataStr = Console.ReadLine();
            var msg = new List<byte>();
            msg.AddRange(Encoding.ASCII.GetBytes(dataStr));
            Console.WriteLine("if add a Enter");
            if (string.Equals(Console.ReadLine(), "y", StringComparison.OrdinalIgnoreCase))
            {
                msg.Add(0x0D);
                msg.Add(0x0A);
            }

            //Console.WriteLine("Try to reverse the command array?");
            //if (string.Equals(Console.ReadLine(), "y", StringComparison.OrdinalIgnoreCase))
            //    msg.Reverse();

            Console.WriteLine("Data to be sent:");
            Console.WriteLine(Encoding.ASCII.GetString(msg.ToArray()));
            Console.WriteLine("Data to be sent in Hex:");
            msg.ForEach(m => Console.Write("{0:X} ", m));
            Console.WriteLine(Environment.NewLine);

            //Console.WriteLine("Write by byte?");
            //if (string.Equals(Console.ReadLine(), "y", StringComparison.OrdinalIgnoreCase))
            //    msg.ForEach(m => serialPort.Write(Encoding.ASCII.GetString(new byte[1] { m })));
            //else
            serialPort.Write(msg.ToArray(), 0, msg.Count);
            //Thread.Sleep(3000);
            //var recieved = new byte[t5Com.BytesToRead];
            //t5Com.Read(recieved, 0, recieved.Length);

            //Console.WriteLine("Data recieved:");
            //Console.WriteLine(Encoding.ASCII.GetString(recieved));
            Thread.Sleep(1000);

            Console.WriteLine("Data read: ");
            var data = serialPort.ReadExisting();
            Console.WriteLine(data);

            Console.WriteLine("Reset COM NO?");

            if (string.Equals(Console.ReadLine(), "y", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine();
                Console.WriteLine();
                goto Restart;
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine();
                goto SameComNO;
            }

            // Console.ReadKey();
        }
    }
}

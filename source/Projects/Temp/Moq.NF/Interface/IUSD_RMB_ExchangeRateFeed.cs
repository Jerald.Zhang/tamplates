﻿namespace Moq.NF.Interface
{
    public interface IUSD_RMB_ExchangeRateFeed
    {
        int GetActualUSDValue();
    }
}

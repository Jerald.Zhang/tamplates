﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SerialPortMoniter
{
    class Program
    {
        public static SerialPort Port { get; set; }
        static void Main(string[] args)
        {
            var comNames = SerialPort.GetPortNames();
            foreach (var comName in comNames)
            {
                Console.WriteLine(comName);
            }
            Console.WriteLine("input a COM No");
            var serialPort = new SerialPort($"COM{int.Parse(Console.ReadLine())}", 9600, Parity.Even, 8, StopBits.One)
            {
                Handshake = Handshake.XOnXOff,
                ReceivedBytesThreshold = 1
            };

            Port = serialPort;
            serialPort.DataReceived += new SerialDataReceivedEventHandler(T5Com_DataReceived);
            serialPort.Open();
            var comStream = serialPort.BaseStream;
            Thread.Sleep(5000);
            var sw = new StreamReader(comStream);
            //var sw = new StreamWriter(Console.OpenStandardOutput());
            Console.SetIn(sw);
            sw.ReadToEnd();
            Console.WriteLine("Bye");
            Console.ReadKey();
        }
        private static void T5Com_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Console.WriteLine($"Recieved: {Port.ReadExisting()}");
        }
    }
}

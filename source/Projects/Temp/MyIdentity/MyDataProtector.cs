﻿using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace MyIdentity
{
    public class MyDataProtector : IDataProtector
    {
        public byte[] Protect(byte[] userData)
        {
            string[] purposes = new string[3] { "1", "2", "3" };
            return MachineKey.Protect(userData, purposes);
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            string[] purposes = new string[3] { "1", "2", "3" };
            return MachineKey.Unprotect(protectedData, purposes);
        }
    }
}

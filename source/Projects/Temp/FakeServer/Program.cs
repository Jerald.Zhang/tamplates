﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FakeServer
{
    class Program
    {
        private static Socket _socket;
        static void Main(string[] args)
        {
            var port = 8080;
            Console.WriteLine($"{IPAddress.Loopback.ToString()}:{port}:");
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), port));
            var listenNum = 3;
            _socket.Listen(listenNum);

            var receivedData = new List<byte>();
            //connected once
            while (true)
            {
                var client = _socket.Accept();
                Task.Run(() =>
                {
                    using (client)
                    {
                        Console.WriteLine("Connected");
                        var receiveBuffer = new byte[256];
                        do
                        {
                            var length = client.Receive(receiveBuffer);
                            receivedData.AddRange(receiveBuffer.Take(length));
                        }
                        while (client.Available >= 1);
                        var receiveString = Encoding.UTF8.GetString(receivedData.ToArray());
                        Console.WriteLine(receiveString);
                        client.Send(Encoding.UTF8.GetBytes($"{DateTime.Now.ToString()}:{receiveString}"));
                        client.Shutdown(SocketShutdown.Both);
                    }
                });
            }

            //keep conntected
            //while (true)
            //{
            //    using (var client = _socket.Accept())
            //    {
            //        Console.WriteLine("Connected");
            //        var receiveBuffer = new byte[256];
            //        while (true)
            //        {
            //            try
            //            {
            //                do
            //                {
            //                    var length = client.Receive(receiveBuffer);
            //                    receivedData.AddRange(receiveBuffer.Take(length));
            //                }
            //                while (client.Available > 0);
            //                var receiveString = Encoding.ASCII.GetString(receivedData.ToArray());
            //                Console.WriteLine(receiveString);
            //                client.Send(receivedData.ToArray());

            //                receivedData.Clear();
            //                if (string.Equals(receiveString, "q"))
            //                    break;
            //            }
            //            catch (Exception)
            //            {

            //                break;
            //            }
            //        }
            //    }
            //}
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyContainer
{
    public class TwoDependency
    {
        public TwoDependency()
        {
            IntValue = int.MaxValue;
            StringValue = nameof(TwoDependency);
        }
        public int IntValue { get; set; }
        public string StringValue { get; set; }
    }
}

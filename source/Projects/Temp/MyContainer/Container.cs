﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyContainer
{
    /// <summary>
    /// Ignore Multi-Thread
    /// </summary>
    public class Container
    {
        public Container()
        {
            InstanceContainer = new List<Element>();
        }

        private List<Element> InstanceContainer { get; set; }

        //none parameter constructor
        public Element RegisterType<T>()
        {
            var instanceType = typeof(T);
            var instance = CreateInstance(instanceType);
            return AddInstance(instanceType, instance);
        }

        public Element RegisterInstance<T>(T instance) where T : class
        {
            var instanceType = instance.GetType();
            return AddInstance(instanceType, instance);
        }

        //public Object Resolver<T>()
        //{

        //}

        //public Object Resolver(Type instanceType)
        //{

        //}

        private Element AddInstance(Type instanceType,object instance)
        {
            var element = InstanceContainer.Where(e => e.InstancType == instanceType).FirstOrDefault();
            if (element == null)
            {
                element = new Element(instanceType, instance);
                InstanceContainer.Add(element);
            }
            else
                element.Instance = instance;

            return element;
        }

        private Element AddInstance(Type instanceType, Func<object> creator)
        {
            var element = InstanceContainer.Where(e => e.InstancType == instanceType).FirstOrDefault();
            if (element == null)
            {
                element = new Element(instanceType, creator);
                InstanceContainer.Add(element);
            }
            else
                element.Creator = creator;

            return element;
        }

        private object FindDependency(Type instanceType)
        {
            var element = InstanceContainer.Where(c => c.InstancType == instanceType).FirstOrDefault();
            if (element != null)
                return element.Instance;
            else
                return null;
        }

        private object CreatInstance(Type instanceType)
        {
            var constructorInfos = instanceType.GetConstructors().ToList();
            var maxParameterConstructor = constructorInfos
                .OrderByDescending(ctr => ctr.GetParameters().Count()).First();

            var ctrParameters = maxParameterConstructor.GetParameters().ToList();
            var ctrParameterInstances = new List<object>();
            
            foreach (var param in ctrParameters)
            {
                if (param.ParameterType.IsValueType)
                    throw new InvalidOperationException($"No dependency of paramenter {param.Name} of type {nameof(T)} is found");
                var dependency = FindDependency(instanceType);
                if (dependency != null)
                    ctrParameterInstances.Add(dependency);
                else
                    ctrParameterInstances.Add(CreatInstance<param.ParameterType>());
            }

            return maxParameterConstructor.Invoke(ctrParameters.ToArray());
        }

        private object CreateInstance(Type instanceType)
        {
            if (instanceType.IsValueType)
                throw new InvalidOperationException($"No dependency of paramenter {param.Name} of type {nameof(T)} is found");

            var ctor = implementationType.GetConstructors()
              .OrderByDescending(ctr => ctr.GetParameters().Count()).First();
            var parameterTypes = ctor.GetParameters().Select(p => p.ParameterType);
            var dependencies = parameterTypes.Select(t => CreatInstance(t)).ToArray();
            return Activator.CreateInstance(implementationType, dependencies);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyContainer
{
    public class Element
    {
        public Element(Type type)
        {
            InstancType = type;
        }

        public Element(Type type, object instance)
        {
            InstancType = type;
            Instance = instance;
        }

        public Element(Type type, Func<Object> creator)
        {
            InstancType = type;
            Creator = creator;
        }

        public Type InstancType { get; set; }

        public object Instance { get; set; }

        public Func<Object> Creator { get; set; }
    }
}

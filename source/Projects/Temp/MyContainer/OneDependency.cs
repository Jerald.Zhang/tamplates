﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyContainer
{
    public class OneDependency
    {
        public OneDependency()
        {
            IntValue = int.MaxValue;
            StringValue = nameof(OneDependency);
        }
        public int IntValue { get; set; }
        public string StringValue { get; set; }
    }
}

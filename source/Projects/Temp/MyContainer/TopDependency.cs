﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyContainer
{
    public class TopDependency
    {
        public TopDependency()
        {

        }

        public TopDependency(OneDependency one)
        {
            One = one;
        }

        public TopDependency(TwoDependency two)
        {
            Two = two;
        }

        public TopDependency(OneDependency one, TwoDependency two)
        {
            One = one;
            Two = two;
        }

        public OneDependency One { get; set; }
        public TwoDependency Two { get; set; }
    }
}

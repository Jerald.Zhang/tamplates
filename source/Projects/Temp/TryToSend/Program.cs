﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TryToSend
{
    class Program
    {
        static void Main(string[] args)
        {
            var serialPort = new SerialPort($"COM4", 115200, Parity.None, 8, StopBits.One)
            {
                //Handshake = Handshake.None,//no influence
                //BreakState = true,// Exception will throw if true or false
                //RtsEnable = true,//no influence
                DtrEnable = true
            };

            if (!serialPort.IsOpen)
                serialPort.Open();

            var command = "||>TRIGGER ON\r\n";

            //You can sent the bytes each by each it's the same
            var request = Encoding.ASCII.GetBytes(command);
            serialPort.Encoding = Encoding.ASCII;
            serialPort.WriteTimeout = 2000;
            serialPort.Write(request, 0, request.Length);

            Thread.Sleep(2000);
            Console.WriteLine("Data read: ");
            var respond = new List<byte>();
            var recieveBuffer = new byte[128];
            var readLenght = 0;
            while (serialPort.BytesToRead > 0)
            {
                readLenght = serialPort.Read(recieveBuffer, 0, recieveBuffer.Length);
                if (readLenght < recieveBuffer.Length)
                    respond.AddRange(recieveBuffer.Take(readLenght));
                else
                    respond.AddRange(recieveBuffer);
            }

            Console.Write(Encoding.ASCII.GetString(respond.ToArray()));
            serialPort.Close();
            Console.WriteLine("END");
            Console.ReadKey();
        }
        //public static Task<string> GetDevicePortName()
        //{
        //                var serialPort = new SerialPort($"COM4", 115200, Parity.None, 8, StopBits.One)
        //    {
        //        //Handshake = Handshake.None,//no influence
        //        //BreakState = true,// Exception will throw if true or false
        //        //RtsEnable = true,//no influence
        //        DtrEnable = true
        //    };

        //    if (!serialPort.IsOpen)
        //        serialPort.Open();

        //    var command = "||>TRIGGER ON\r\n";

        //    //You can sent the bytes each by each it's the same
        //    var request = Encoding.ASCII.GetBytes(command);
        //    serialPort.Encoding = Encoding.ASCII;
        //    serialPort.WriteTimeout = 2000;
        //    serialPort.Write(request, 0, request.Length);
        //    var taskCompletionSource = new TaskCompletionSource<string>();
        //    serialPort.DataReceived += (s, e) =>
        //    {
        //        var dataIn = (byte)serialPort.ReadByte();
        //        var receivedCharacter = Convert.ToChar(dataIn);
        //        if (receivedCharacter == Constants.DeviceConstants.SignalYes)
        //        {
        //            serialPort.Dispose();
        //            taskCompletionSource.SetResult(serialPort.PortName);
        //        }
        //    };
        //    foreach (var port in ports)
        //    {
        //        serialPort.PortName = port;
        //        try
        //        {
        //            serialPort.Open();
        //            serialPort.Write(Constants.DeviceConstants.SignalDeviceDetect);
        //        }
        //        catch (IOException e) { }
        //    }
        //    return taskCompletionSource.Task;
        //}
    }
}

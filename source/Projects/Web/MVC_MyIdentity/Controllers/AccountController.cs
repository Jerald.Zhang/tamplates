﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataProtection;
using MyIdentity;

namespace MVC_MyIdentity.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            //基础数据 string, int ...
            var dictionary = new Dictionary<string, string>();

            //Cookie 加密解密的对象
            var identity = new ClaimsIdentity();
            var properties = new AuthenticationProperties(dictionary);
            var authenticationTicket = new AuthenticationTicket(identity, properties);
            IDataProtector protector = new MyDataProtector();
            ISecureDataFormat<AuthenticationTicket> dataFormater = new TicketDataFormat(protector);
            return View();
        }
    }
}
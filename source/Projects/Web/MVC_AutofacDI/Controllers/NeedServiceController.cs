﻿using MVC_AutofacDI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Core_MVC.Controllers
{
    public class NeedServiceController : Controller
    {
        public NeedServiceController(INeedService needService)
        {
            NeedService = needService;
        }
        public INeedService NeedService { get; set; }

        public ActionResult Index()
        {
            var test = NeedService.GetAString();
            return View((object)test);
        }
    }
}
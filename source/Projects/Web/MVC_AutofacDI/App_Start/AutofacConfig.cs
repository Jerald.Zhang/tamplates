﻿using Autofac;
using Autofac.Integration.Mvc;
using MVC_AutofacDI.Interfaces;
using MVC_AutofacDI.Models;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace MVC_AutofacDI
{
    public class AutofacConfig
    {
        // For more information see :https://code.google.com/p/autofac/wiki/MvcIntegration (mvc4 instructions)
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterInstance(new NeedService()).As<INeedService>();
            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            // builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            // builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            // builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            // builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            // builder.RegisterFilterProvider();

            // OPTIONAL: Enable action method parameter injection (RARE).
            // builder.InjectActionInvoker();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
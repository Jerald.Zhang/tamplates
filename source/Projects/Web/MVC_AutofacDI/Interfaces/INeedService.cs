﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_AutofacDI.Interfaces
{
    public interface INeedService
    {
        string GetAString();
    }
}

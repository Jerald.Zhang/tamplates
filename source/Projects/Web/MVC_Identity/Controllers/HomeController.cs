﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC_Identity.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //throw new Exception("Test");//在这里直接抛出异常,如果不注册异常处理Filter那么就会发生运行时异常,如果注册了异常处理Filter,则交给这个Filter处理
            var user = HttpContext.User;
            var cookes = HttpContext.Request.Cookies;
            var isAdmin = user.IsInRole("Admin");

            //Cookie 加密解密的对象
            var identity = new ClaimsIdentity();
            //var authenticationTicket = new AuthenticationTicket();
            //IDataProtector protector = MachineKey
            //ISecureDataFormat<AuthenticationTicket> dataFormater = new TicketDataFormat(protector);
            //CookieAuthenticationHandler
            return View();
        }
        [Authorize(Roles = "User")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
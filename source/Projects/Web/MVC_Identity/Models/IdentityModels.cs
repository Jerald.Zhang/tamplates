﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MVC_Identity.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class ApplicationInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext dbContext)
        {
            base.Seed(dbContext);
            dbContext.Users.Add(new ApplicationUser
            {
                Id= "f041da51-493c-4289-aa29-245aaa2bdb11",
                AccessFailedCount = 5,
                Email = "He.Zhang@emerson.com",
                UserName= "He.Zhang@emerson.com",
                PasswordHash = "ALkxV+y/W1vQqlKkYmu9+NNrFlDOP9e4njRR5r4UwfrkBVt2fTa6sL7EEWpj7hPiTA==",
                SecurityStamp= "c47c208d-a576-4a61-ae15-110652e7a069",
                PhoneNumber="13266666666"
            });
            dbContext.SaveChanges();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Core_MVC.Controllers
{
    public class HomeAsyncController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
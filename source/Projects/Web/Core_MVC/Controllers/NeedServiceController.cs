﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_MVC.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Core_MVC.Controllers
{
    public class NeedServiceController : Controller
    {
        public NeedServiceController(INeedService needService)
        {
            Service = needService;
        }
        public INeedService Service { get; set; }
        public IActionResult Index()
        {
            return View();
        }
    }
}
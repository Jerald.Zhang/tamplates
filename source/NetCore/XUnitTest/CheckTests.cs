﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace XUnitTest
{
    public class DataCheckTests
    {
        [Theory]
        [InlineData(new byte[] { 0x82 }, 0x82)]
        [InlineData(new byte[] { 0x02, 0x80, 0x00, 0x00 }, 0x82)]
        [InlineData(new byte[] { 0x86, 0xa6, 0x18, 0x4c, 0x5e, 0xc1, 0x0d, 0x17, 0x00, 0x40, 0x82, 0x08, 0x20, 0x82, 0x08, 0x20, 0x82, 0x08, 0x20, 0x82, 0x08, 0x20, 0x82, 0x08, 0x20, 0x82, 0x08, 0x20, 0x1e, 0x07, 0x71 }, 0xD9)]
        public void XORTests(byte[] data, byte expected)
        {
            var actual = 0x00;
            foreach (var item in data)
            {
                actual ^= item;
            }

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(new byte[] { 0x82 }, new byte[] { 0x82 })]
        public void CRCTests_CRC16Modbus(byte[] data, byte[] expected)
        {
            //if (expected.Length != 2)
            //    throw new ArgumentException();
            //byte POLY;
            //var actual = new byte[2];
            //foreach (var item in data)
            //{
            //    actual ^= item;
            //}

            //Assert.Equal(expected, actual);
        }
    }
}

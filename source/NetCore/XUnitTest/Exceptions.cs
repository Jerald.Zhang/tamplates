﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace XUnitTest
{
    public class Exceptions
    {
        public Exceptions(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        private readonly ITestOutputHelper _testOutputHelper;

        [Fact]
        public void WhatsAggregateException()
        {

        }
        [Fact]
        public void TypeConvertException()
        {
            try
            {
                dynamic obj = "82";
                var ifTrue = (int)obj;
            }
            catch (Exception ex)
            {
                var type = ex.GetType();
            }
        }
    }
}

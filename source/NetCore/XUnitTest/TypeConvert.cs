﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using XUnitTest.Model.TypeConvert;

namespace XUnitTest
{
    public class TypeConvert
    {
        [Fact]
        public void WhichException()
        {
            var tOne = GetTypeOne();
            var tTwo =  (TypeTwo)tOne;
        }

        public static IType GetTypeOne()
        {
            return new TypeOne(); 
        }
    }
}

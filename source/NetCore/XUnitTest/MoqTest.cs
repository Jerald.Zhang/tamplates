﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using XUnitTest.Model.Moq;

namespace XUnitTest
{
    public class MoqTest
    {
        [Fact]
        public void MustHaveVirtual()
        {
            var moqTest = new Mock<MoqTestImp>();
            moqTest.Setup(m => m.MoqTestMethodString()).Returns("2");

            Assert.Throws<Exception>(() => moqTest.Object.MoqTestMethodString());
        }
    }
}

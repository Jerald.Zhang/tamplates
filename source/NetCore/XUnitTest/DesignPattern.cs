﻿using System;
using System.Collections.Generic;
using System.Text;
using XUnitTest.Model.DesignPattern.Adapter;

namespace XUnitTest
{
    public class DesignPattern
    {
        public void Adapter()
        {
            var android = new Android();
            var iPhone = new Iphone();
            var lightningPhoneWithMicroUsbAdapter = new LightningToMicroUsbAdapter(iPhone);

            Console.WriteLine("Recharging android with MicroUsb");
            AdapterDemo.RechargeMicroUsbPhone(android);

            Console.WriteLine("Recharging iPhone with Lightning");
            AdapterDemo.rechargeLightningPhone(iPhone);

            Console.WriteLine("Recharging iPhone with MicroUsb");
            AdapterDemo.RechargeMicroUsbPhone(lightningPhoneWithMicroUsbAdapter);
        }
    }
}

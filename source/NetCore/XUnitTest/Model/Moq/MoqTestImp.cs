﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XUnitTest.Model.Moq
{
    public class MoqTestImp : IMoqTest
    {
        public virtual int MoqTestMethod()
        {
            return 1;
        }

        public string MoqTestMethodString()
        {
            return "1";
        }
    }
}

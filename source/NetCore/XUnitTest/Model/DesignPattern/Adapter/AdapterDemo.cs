﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XUnitTest.Model.DesignPattern.Adapter
{
    public class AdapterDemo
    {
        public static void RechargeMicroUsbPhone(IMicroUsb phone)
        {
            phone.ConnectMicroUsb();
            phone.Recharge();
        }

        public static void rechargeLightningPhone(ILightning phone)
        {
            phone.ConnectLightning();
            phone.Recharge();
        }
    }
}

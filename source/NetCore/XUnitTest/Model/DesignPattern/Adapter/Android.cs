﻿using System;

namespace XUnitTest.Model.DesignPattern.Adapter
{
    public class Android : IMicroUsb
    {
        private bool connector;

        public void ConnectMicroUsb()
        {
            connector = true;
            Console.WriteLine("MicroUsb connected");
        }

        public void Recharge()
        {
            if (connector)
            {
                Console.WriteLine("Recharge started");
                Console.WriteLine("Recharge finished");
            }
            else
            {
                Console.WriteLine("Connect MicroUsb first");
            }
        }
    }
}
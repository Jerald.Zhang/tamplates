﻿namespace XUnitTest.Model.DesignPattern.Adapter
{
    public interface IMicroUsb
    {
        void Recharge();
        void ConnectMicroUsb();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XUnitTest.Model.DesignPattern.Adapter
{
    public class LightningToMicroUsbAdapter : IMicroUsb
    {
        private readonly ILightning lightningPhone;

        public LightningToMicroUsbAdapter(ILightning lightningPhone)
        {
            this.lightningPhone = lightningPhone;
        }


        public void ConnectMicroUsb()
        {
            Console.WriteLine("MicroUsb connected");
            lightningPhone.ConnectLightning();
        }


        public void Recharge()
        {
            lightningPhone.Recharge();
        }
    }
}

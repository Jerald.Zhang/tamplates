﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XUnitTest.Model.DesignPattern.Adapter
{
    public class Iphone : ILightning
    {
        private bool connector;

        public void ConnectLightning()
        {
            connector = true;
            Console.WriteLine("Lightning connected");
        }

        public void Recharge()
        {
            if (connector)
            {
                Console.WriteLine("Recharge started");
                Console.WriteLine("Recharge finished");
            }
            else
            {
                Console.WriteLine("Connect Lightning first");
            }
        }
    }
}

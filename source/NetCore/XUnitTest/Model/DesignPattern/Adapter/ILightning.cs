﻿namespace XUnitTest.Model.DesignPattern.Adapter
{
    public interface ILightning
    {
        void Recharge();
        void ConnectLightning();
    }
}
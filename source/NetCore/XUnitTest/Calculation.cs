﻿using Xunit;

namespace NetCoreUnitTest
{
    public class Calculation
    {
        [Theory]
        [InlineData(true, true)]
        [InlineData(true, false)]
        [InlineData(false, true)]
        [InlineData(false, false)]
        public void DeMorganLaws(bool a, bool b)
        {
            Assert.Equal(!a && !b, !(a || b));
        }
    }
}

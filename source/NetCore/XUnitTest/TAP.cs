﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace XUnitTest
{
    /// <summary>
    /// Task-based asynchronous pattern (TAP)
    /// </summary>
    public class TAP
    {
        public TAP(ITestOutputHelper testOutputHelper)
        {
            TestOutputHelper = testOutputHelper;
        }

        private ITestOutputHelper TestOutputHelper { get; }

        [Fact]
        public void Wait_WillThrowExceptionInCallerThread()
        {
            var task = Task.Run(() =>
            {
                throw new NotImplementedException();
            });

            try
            {
                task.Wait();
                Assert.True(false);
            }
            catch (Exception)
            {
                TestOutputHelper.WriteLine(task.Exception.Message);
                Assert.True(true);
            }

            Assert.True(task.IsFaulted);
        }
    }
}

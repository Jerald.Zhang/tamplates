﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace XUnitTest
{
    public class Binary
    {
        [Fact]
        public void Character()
        {
            var defaultUnicodeChar = '某';
            var value = (int)defaultUnicodeChar;
            Console.WriteLine(value);
            //var asciiChar = Encoding.ASCII.GetBytes();
        }

        [Theory]
        [InlineData(1, new byte[] { 0, 0, 0, 1 })]
        [InlineData(-1, new byte[] { 255, 255, 255, 255 })]
        [InlineData(-255, new byte[] { 255, 255, 255, 1 })]
        public void IsIntStoredInBLSComplementFormat(int intValue, byte[] bytesOfIntBLSExpected)
        {
            var bytesOfInt = BitConverter.GetBytes(intValue);
            if (BitConverter.IsLittleEndian)
                bytesOfIntBLSExpected = bytesOfIntBLSExpected.Reverse().ToArray();
            Assert.True(Enumerable.SequenceEqual(bytesOfInt, bytesOfIntBLSExpected));

            var actualValue = BitConverter.ToInt32(bytesOfIntBLSExpected, 0);
            Assert.Equal(intValue, actualValue);
        }
    }
}
